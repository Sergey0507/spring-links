package spring;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import spring.config.*;

public class ServletInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[]{WebConfig.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/*"};
    }

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[]{AppConfig.class};
    }

//    @Override
//    protected Class<?>[] getRootConfigClasses() {
//        return new Class<?>[]{AppConfig.class, HiberConfig.class, RestWebSecurityConfig.class};
//    }
// если @Profile("!spring-data-jpa") выключен нкжно поменять RestWebSecurityConfig.class на WebSecurityConfig.class
}
