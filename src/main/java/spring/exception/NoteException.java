package spring.exception;

import org.springframework.http.HttpStatus;

public class NoteException extends RuntimeException {

    private HttpStatus status;

    public NoteException(HttpStatus status, String message) {
        super(message);
        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }

}
