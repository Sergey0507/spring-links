package spring.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import spring.persistence.entity.LinksEntity;
import spring.persistence.repository.projection.LinksName;

import java.util.List;
import java.util.Optional;


@Repository
public interface UrlRepository extends JpaRepository<LinksEntity, Integer> {
    Optional<LinksEntity> findByName(String name);

    Optional<List<LinksEntity>> findByNameIsContaining(String param);

    @Query(value = "SELECT n.name FROM links AS n", nativeQuery = true)
    List<LinksName> findAllOnlyWithNames();

}
