package spring.persistence.entity;

import javax.persistence.*;

@Entity
@Table(name = "links")
public class LinksEntity {

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Integer id;

@Column(length = 256)
private String name;

@Column(nullable = false, unique = true)
private String url;

    public LinksEntity() {
    }

    public LinksEntity( String name, String url) {
        this.name = name;
        this.url = url;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

}
