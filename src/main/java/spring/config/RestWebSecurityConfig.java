package spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.NegatedRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import spring.security.filter.JwtTokenFilter;

// -Dspring.profiles.active=spring-data-jpa
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,
//        jsr250Enabled = true,
        prePostEnabled = true
)

@ComponentScan("spring.security")
public class RestWebSecurityConfig extends WebSecurityConfigurerAdapter {

    public static final RequestMatcher PUBLIC_URLS = new OrRequestMatcher(
//            new AntPathRequestMatcher("/spring/sp/*"),
            new AntPathRequestMatcher("/spring/sp/auth"),
            new AntPathRequestMatcher("/spring/sp/auth/*")
    );

    public static final RequestMatcher PROTECTED_URLS = new NegatedRequestMatcher(PUBLIC_URLS);

    private final JwtTokenFilter jwtTokenFilter;

    public RestWebSecurityConfig(JwtTokenFilter jwtTokenFilter) {
        this.jwtTokenFilter = jwtTokenFilter;
    }

    @Bean
    public PasswordEncoder passwordEncoder () {
        return new BCryptPasswordEncoder ();
    }

//    @Bean
//    public UserDetailsService userDetailsService() {
//        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
//        manager.createUser(User.withDefaultPasswordEncoder().
//                username("user").
//                password("password")
////                .passwordEncoder(nashPasword -> passwordEncoder().encode(nashPasword))
//                .passwordEncoder(new Function<String, String>() {
//                    @Override
//                    public String apply(String nashPasword) {
//                        return passwordEncoder().encode(nashPasword);
//                    }
//                })
//                .roles("USER").build());
//        return manager;
//    }


    @Override
    public void configure(WebSecurity web) throws Exception {

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.cors().disable()
                .csrf().disable()
                .logout().disable()
                .formLogin().disable();

        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .disable();

        http.authorizeRequests()
                .requestMatchers(PUBLIC_URLS)
                .permitAll();

        http.authorizeRequests()
                .requestMatchers(PROTECTED_URLS)
                .authenticated();

//        http.httpBasic().disable();

//        http.formLogin().permitAll();

        http.addFilterBefore(this.jwtTokenFilter, UsernamePasswordAuthenticationFilter.class);

    }

}