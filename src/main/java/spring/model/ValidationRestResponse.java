package spring.model;

import java.util.List;

public class ValidationRestResponse extends RestResponse {

    private final List<ValidationFieldError> fields;

    public ValidationRestResponse(String message, List<ValidationFieldError> fields) {
        super(message);
        this.fields = fields;
    }

    public List<ValidationFieldError> getFields() {
        return fields;
    }

    public static ValidationRestResponse with(List<ValidationFieldError> fields) {
        return with("Validation Error", fields);
    }

    public static ValidationRestResponse with(String message, List<ValidationFieldError> fields) {
        return new ValidationRestResponse(message, fields);
    }

}
