package spring.model.pojo;

public class PaginationLinks {

    private int page;

    private int pageSize;

    private int totalPages;

    public PaginationLinks(int page, int pageSize, int totalPages) {
        this.page = page;
        this.pageSize = pageSize;
        this.totalPages = totalPages;
    }

    public int getPage() {
        return page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public int getTotalPages() {
        return totalPages;
    }

}
