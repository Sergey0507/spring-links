package spring.model.pojo;

public class NoteURL {

   private String name;

   private String url;

    public NoteURL() {
    }

    public NoteURL(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String toString() {
        return "NoteURL{" +
                "name='" + name + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
