package spring.model.pojo;

import java.util.List;

public class PageLinks {

    private List<NoteURL> pageItems;

    private PaginationLinks paginationLinks;

    public PageLinks(List<NoteURL> pageItems, PaginationLinks paginationLinks) {
        this.pageItems = pageItems;
        this.paginationLinks = paginationLinks;
    }

    public List<NoteURL> getPageItems() {
        return pageItems;
    }

    public PaginationLinks getPaginationLinks() {
        return paginationLinks;
    }

}
