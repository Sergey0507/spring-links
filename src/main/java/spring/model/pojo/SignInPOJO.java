package spring.model.pojo;

public class SignInPOJO {

    private String login;

    private String password;

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

}
