package spring.model.pojo;

public class AuthPOJO {

    private final Long id;

    private final String login;

    private final String accessToken;

    private final String refreshToken;


    public AuthPOJO(Long id, String login, String accessToken, String refreshToken) {
        this.id = id;
        this.login = login;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    public Long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }
}
