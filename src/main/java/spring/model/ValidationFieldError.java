package spring.model;

public class ValidationFieldError {

    private String name;

    private String message;

    public ValidationFieldError(String name, String message) {
        this.name = name;
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public String getMessage() {
        return message;
    }

}
