package spring.model;

public class RestResponse {

    private String message;

    public RestResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public static RestResponse with(String message) {
        return new RestResponse(message);
    }

}
