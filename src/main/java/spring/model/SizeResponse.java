package spring.model;

public class SizeResponse {

    String nane;

    int size;

    public SizeResponse(String nane, int size) {
        this.nane = nane;
        this.size = size;
    }

    public String getNane() {
        return nane;
    }

    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        return "SizeResponse{" +
                "nane='" + nane + '\'' +
                ", size=" + size +
                '}';
    }
}
