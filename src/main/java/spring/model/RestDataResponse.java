package spring.model;

public class RestDataResponse extends RestResponse {

    private final Object data;

    public RestDataResponse(Object data) {
        this(null, data);
    }

    public RestDataResponse(String message, Object data) {
        super(message);
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public static RestDataResponse with(Object data) {
        return new RestDataResponse(data);
    }

}
