package spring.web.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import spring.model.pojo.NoteURL;

@Component
public class ValidatorURL implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass == NoteURL.class;
    }

    @Override
    public void validate(Object url, Errors errors) {

        System.out.println("Validator");
        NoteURL noteURL = (NoteURL) url;

       if (!noteURL.getUrl().startsWith("https://")){
           errors.rejectValue("url","","ERROR !!!");
           System.out.println("ERRPR !!!! ");
       }
    }
}
