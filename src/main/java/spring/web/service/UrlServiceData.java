package spring.web.service;

import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spring.persistence.entity.LinksEntity;
import spring.model.pojo.NoteURL;
import spring.persistence.repository.projection.LinksName;
import spring.persistence.repository.UrlRepository;

import javax.servlet.ServletContext;
import java.util.List;
import java.util.Optional;
// -Dspring.profiles.active=spring-data-jpa
@Service
@Transactional
public class UrlServiceData {

    public final ServletContext context;
    public final UrlRepository urlRepository;

    public UrlServiceData(ServletContext context, UrlRepository urlRepository) {
        this.context = context;
        this.urlRepository = urlRepository;
    }

    public ResponseEntity<List<LinksEntity>> getAll(){
        return ResponseEntity.ok(urlRepository.findAll());
    }

    public Optional<LinksEntity> getOne (Integer id){
        return urlRepository.findById(id);
    }

    public List<LinksName> getUrlNames(){
        return urlRepository.findAllOnlyWithNames();
    }

    public Optional<LinksEntity> getName (String name){
        return urlRepository.findByName(name);
    }

    public ResponseEntity <List<LinksEntity>> getAllpagination (Integer page, Integer pageSize){
        Page <LinksEntity> pagination = urlRepository.findAll(PageRequest.of(page, pageSize));
        return ResponseEntity.ok(pagination.getContent());
    }

    public Optional<List<LinksEntity>> getSearch (String name){
        return urlRepository.findByNameIsContaining(name);
    }

    public ResponseEntity create (NoteURL url){
        LinksEntity noteURL = new LinksEntity(url.getName(), url.getUrl());
        urlRepository.save(noteURL);
        return ResponseEntity.ok(url);
    }

    public void delOne(int id){
        urlRepository.deleteById(id);
    }

    public void delAll(){
        urlRepository.deleteAll();
    }

}
