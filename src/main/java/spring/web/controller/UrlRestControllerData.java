package spring.web.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import spring.persistence.entity.LinksEntity;
import spring.model.pojo.NoteURL;
import spring.persistence.repository.projection.LinksName;
import spring.web.service.UrlServiceData;
import spring.web.validator.ValidatorURL;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
// -Dspring.profiles.active=spring-data-jpa
@RestController
@RequestMapping("/spring/sp")
public class UrlRestControllerData {

    private final UrlServiceData service;

    private final ValidatorURL validatorURL;

    public UrlRestControllerData(UrlServiceData service, ValidatorURL validatorURL) {
        this.service = service;
        this.validatorURL = validatorURL;
    }

    @InitBinder
    public void noteInitBinder(WebDataBinder binder) {
        binder.addValidators(this.validatorURL);
    }

    @GetMapping(path = "/all")
    public ResponseEntity<List<LinksEntity>> getAll(Authentication authentication) {
        System.out.println("GET_ALL");
        System.out.println(authentication);
//        System.out.println(((User) authentication.getPrincipal()).getUsername());
        return service.getAll();
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Optional<LinksEntity>> getOne(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(service.getOne(id));
    }

    @GetMapping(path = "name/{name}")
    public ResponseEntity<Optional<LinksEntity>> getName(@PathVariable("name") String name) {
        return ResponseEntity.ok(service.getName(name));
    }

    @GetMapping("/names")
    public ResponseEntity<List<LinksName>> getUrlNames() {
        return ResponseEntity.ok(service.getUrlNames());
    }

    @GetMapping(path = "/all_pagin/{page}")
    public ResponseEntity<List<LinksEntity>> getAllpagination(@PathVariable("page") Integer page,
                                                      @RequestParam(value = "pageSize") Integer pageSize) {
        System.out.println("GET_ALL_PAGINATION");
        return service.getAllpagination(page, pageSize);
    }

    @GetMapping(path = "/search")
    public Optional<List<LinksEntity>> getsearch(@RequestParam(value = "param") String param){
        System.out.println("GET_search -> param = " + param);
        return service.getSearch(param);
    }

    @PostMapping
    public ResponseEntity create(@RequestHeader("User-Agent") String userAgent, @Valid @RequestBody NoteURL url) {
        System.out.println("userAgent -> " + userAgent);
        return service.create(url);
    }

    @DeleteMapping(path = "/{id}")
    public void delOne(@PathVariable("id") Integer id) {
        System.out.println("DELIT -> id = " + id);
        service.delOne(id);
    }

    @DeleteMapping("/")
    public void delAll(){
        service.delAll();
    }

}
