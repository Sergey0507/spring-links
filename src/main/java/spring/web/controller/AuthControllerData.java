package spring.web.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import spring.exception.NoteException;
import spring.model.pojo.SignInPOJO;
import spring.persistence.entity.UserEntity;
import spring.model.RestDataResponse;
import spring.model.RestResponse;
import spring.model.pojo.AuthPOJO;
import spring.persistence.repository.UserRepository;
import spring.security.JwtUtils;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

// -Dspring.profiles.active=spring-data-jpa
@RestController
@RequestMapping(value = "/spring/sp/auth",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE )
public class AuthControllerData {

    private final JwtUtils jwtUtils;

    private final UserRepository userRepository;

    public final PasswordEncoder passwordEncoder;

    public AuthControllerData(JwtUtils jwtUtils, UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.jwtUtils = jwtUtils;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

        //создаем пробного юзера если в БД пусто
        @PostConstruct
        public void postInit(){
            if (userRepository.count() <= 0){
                userRepository.save(new UserEntity("user", passwordEncoder.encode("password")));
            }
        }

    @PostMapping
    public ResponseEntity<RestResponse> auth(@RequestBody SignInPOJO signPOJO) {

        Optional<UserEntity> user = userRepository.findByLogin(signPOJO.getLogin());

        if(!user.isPresent()){
            System.out.printf("Пользователь не найден !");
            throw new UsernameNotFoundException("Пользователь не найден !!!");
        }

        Map<String, Object> data = new HashMap<>();
        data.put("login", user.get().getLogin());

        String accessToken = jwtUtils.createToken(data);
        String refreshToken = jwtUtils.createToken(data, true);

        return ResponseEntity.ok(RestDataResponse.with(new AuthPOJO(
               user.get().getId(),
               user.get().getLogin(),
               accessToken,
               refreshToken
        )));
    }

    @PostMapping(path = "/reg")
    public ResponseEntity<RestResponse> registration (@RequestBody SignInPOJO signInPOJO){

        String login = signInPOJO.getLogin();
        String password = signInPOJO.getPassword();
        System.out.println(String.format("Login -> %s Password -> %s", login, password));

        if (login == null || login.length() < 3){
            return ResponseEntity.ok(RestResponse.with("Login введен не корректно"));
        }
        if (password == null || password.length() < 3){
            return ResponseEntity.ok(RestResponse.with("Password введен не корректно"));
        }

        Optional<UserEntity> user = userRepository.findByLogin(signInPOJO.getLogin());
        if (user.isPresent()){
            return ResponseEntity.ok(RestResponse.with("Такой пользователь уже зарегестрирован, введите другой Login"));

        }

        userRepository.save(new UserEntity(login, passwordEncoder.encode(password)));
        return ResponseEntity.ok(RestResponse.with("Регестрацыя успешна !!!"));
    }
}
