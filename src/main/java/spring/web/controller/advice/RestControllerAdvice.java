package spring.web.controller.advice;

import org.springframework.lang.Nullable;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import spring.exception.NoteException;
import spring.model.ValidationFieldError;
import spring.model.RestResponse;
import spring.model.ValidationRestResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@ControllerAdvice
@org.springframework.web.bind.annotation.RestControllerAdvice
public class RestControllerAdvice extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return handleExceptionInternal(ex, RestResponse.with("Message Not Readable"), headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        List<ValidationFieldError> errors = new ArrayList<>();

        for (ObjectError error : ex.getAllErrors()) {

            FieldError fieldError = (FieldError) error;

            errors.add(new ValidationFieldError(fieldError.getField(), fieldError.getDefaultMessage()));

        }

        return super.handleExceptionInternal(ex, ValidationRestResponse.with(errors), headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, @Nullable Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
            request.setAttribute("javax.servlet.error.exception", ex, 0);
        }

        return new ResponseEntity(Objects.nonNull(body) ? body : RestResponse.with(ex.getMessage()), headers, status);
    }

//    ------------------------------------------------------------------------------------------------------------------
//    тоже самое с помощью Stream API

//      @Override
//    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
//        return ResponseEntity.badRequest().body(
//                ValidationRestResponse.with(
//                        ex.getAllErrors()
//                                .stream()
//                                .map(objectError -> (org.springframework.validation.FieldError) objectError)
//                                .map(fieldError -> new ValidationFieldError(fieldError.getField(), fieldError.getDefaultMessage()))
//                                .collect(Collectors.toList())
//                )
//        );
//    }
//    ------------------------------------------------------------------------------------------------------------------

    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return ResponseEntity.badRequest().body(RestResponse.with(ex.getMessage()));
    }

    @ExceptionHandler(NoteException.class)
    public ResponseEntity<RestResponse> handleNoteException(NoteException e) {
        return ResponseEntity.status(e.getStatus()).body(RestResponse.with(e.getMessage()));
    }
}
