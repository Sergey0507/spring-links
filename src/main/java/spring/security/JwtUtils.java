package spring.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Component
@Configuration
@PropertySource("classpath:jwt.properties")
public class JwtUtils {

    @Value("${security.jwt.issuer}")
    private String issuer;

    @Value("${security.jwt.secret-key}")
    private String secretKey;

    @Value("${security.jwt.clock-skew-ses}")
    private int clockSkewSes;

    @Value("${security.jwt.auth-prefix}")
    private String authPrefix;

    @Value("${security.jwt.access-expire-length}")
    private int accessExpireLength;

    @Value("${security.jwt.refresh-expery-length}")
    private int refreshExperyLength;

    @PostConstruct
    public void postConstruct(){
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
    }

    public String createToken(Map<String, Object> userData){
        return createToken(userData, false);
    }

    public String createToken(Map<String, Object> userData, boolean isRefresh){

        Claims claims = createDefaultClaims(userData);

        if (isRefresh){
            claims.put("refresh", true);
        }

        Calendar calendar = Calendar.getInstance();
        Date now = calendar.getTime();
        calendar.add(Calendar.MILLISECOND, isRefresh ? refreshExperyLength : accessExpireLength);

        return compact(claims, now, calendar.getTime());

    }

    public String resolveToken(String token){
        if (token != null && token.startsWith(authPrefix)){
            return token.substring(authPrefix.length() + 1);
        }
        return null;
    }

    public String resolveToken(HttpServletRequest req){
        return resolveToken(req.getHeader(AUTHORIZATION));
    }

    public String get(String key, String token) {
        try {
            return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().get(key, String.class);
        } catch (JwtException | IllegalArgumentException e) {
            System.err.println("ЭЛЕКТРОННАЯ ПОЧТА НЕ НАЙДЕНА В ТОКЕНЕ " + token);
            e.printStackTrace();
        }
        return null;
    }

    public Claims getTokenBoby(String token) throws JwtException, IllegalArgumentException{
        return Jwts.parser().setSigningKey(secretKey).requireIssuer(issuer).parseClaimsJws(token).getBody();
    }

    public boolean validateToken(String token){

        try {
            Jwts.parser()
                    .setSigningKey(secretKey)
                    .requireIssuer(issuer)
                    .parseClaimsJws(token);
            return true;
        }catch (JwtException ex){
            System.err.println("JWT EXCEPTION (ИСКЛЮЧЕНИЕ JWT !!!)");
            ex.printStackTrace();
        }catch (IllegalArgumentException ex){
            System.err.println("JWT claims string is empty (Строка заявлений JWT пуста !!!).");
            ex.printStackTrace();
        }
        return false;
    }

    private Claims createDefaultClaims(Map<String, Object> userDate){
        return Jwts.claims(userDate).setSubject(RandomStringUtils.randomAlphanumeric(10));
    }

   private String compact (Claims claims, Date issuedAt, Date expiration){
        return Jwts.builder()
                .setClaims(claims)
                .setIssuer(issuer)
                .setIssuedAt(issuedAt)
                .setExpiration(expiration)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
   }

}
